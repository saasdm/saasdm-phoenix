/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:AssetCtrl
 * @description # AssetCtrl Controller of the saasdmPhoenixApp
 */
.controller('AssetCtrl', 
		function($scope, $q, $sdp, $routeParams, $app, $page, $amhNavigator, $rootScope,
				PaginatorParameter) {

	$scope.content = {};
	$scope.relateds = [];
	$scope.relations = [];
	$scope.chain = [];
	$scope.working = true;
	$scope.error = 0;
	$scope.assetId = $routeParams.assetId;
	
	function loadRelatedAssets() {
		var pp = new PaginatorParameter();
		pp.setOrder('modif_dtime', 'd');
		return $sdp.relatedAssetsToAsset($scope.asset, pp)//
		.then(function(clist) {
			$scope.relateds = clist;
		});
	}
	
	function loadRelations() {
		var pp = new PaginatorParameter();
		pp.setOrder('modif_dtime', 'd');
		pp.setFilter('start', $scope.asset.id);
		return $sdp.assetRelations(pp)//
		.then(function(clist) {
			$scope.relations = clist;
		});
	}	
	
	
	function load() {
		$scope.content = {};
		var assetId;
		if ($routeParams.assetId) {
			assetId = $routeParams.assetId;
		} else {
			// error
			return;
		}
		$scope.working = true;
		return $sdp.asset(assetId)//
		.then(function(asset) {
			$scope.asset = asset;
			$scope.working = false;
			$scope.error = 0;
			return $scope.asset;
		}, function(error) {
			console.log('Fail to get asset: ' + error.data.message);
			$scope.error = error.status;
			$scope.working = false;
		})//
		.then(function(asset) {
			if(typeof asset === 'undefined'){
				return;
			}
			$scope.setSeoSetting('asset', $scope.asset);
			var promise1 = $scope.loadContentOfAsset(asset);
			// load hirarchical chain of asset
			var promise2 = $scope.loadHirarchyChain(asset, $scope.chain);
			// load related assets
			var promise3 = loadRelatedAssets();
			// load samples
			var promise4 = loadRelations();
			
			$q.all([promise1, promise2, promise3, promise4])//
			.then(function(){
				window.prerenderReady = true;
			})//
			.catch(function(){
				window.prerenderReady = true;				
			});
		});
	}

	function download(assetId){
		if(!assetId){
			return;
		}
		if($rootScope.app.user.anonymous){
			$scope.goTo('/users/login');
			return;
		}
		$sdp.createLink(assetId)//
		.then(function(link) {
			$scope.goTo('/asset/' + assetId + '/link/' + link.id);			
		}, function(error) {
			console.log('Fail to get link: ' + error.data.message);
			$scope.error = error.status;
			$scope.working = false;
			if(error.data.message !== undefined){
				$scope.errorMessage = error.data.message;
				$amhNavigator.openDialog({
					templateUrl: 'views/dialogs/amd-alert.html',
					config: {
						message: error.data.message
					}
				});
			}
		});
	}
	
	$app
	.scopeMenu($scope)//
	.add({ // edit menu
		priority : 15,
		icon : 'edit',
		label: 'Edit content',
		visible : function() {
			return $scope.app.user.owner;
		},
		action : $scope.toggleEditable
	})//
	.add({ // add new page
		priority : 15,
		icon : 'add_box',
		label: 'Create new content',
		visible : function() {
			return $scope.app.user.owner;
		},
		action : $scope.createNewContent
	})//
	.add({ // save menu
		priority : 10,
		icon : 'save',
		label: 'Save current changes',
		visible : function() {
			return $scope.app.user.owner;
		},
		action : $scope.saveContent
	});
	
	$scope.download = download;
	$scope.load = load;

	load();
	
	$scope.$watch('contentValue', function(){
		if($scope.contentValue !== undefined && $scope.contentValue !== null){
			$scope.setSeoSetting('content', $scope.contentValue);
		}
	}, true);
	
});
