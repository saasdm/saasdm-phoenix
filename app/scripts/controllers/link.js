/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:LinkPaymentCtrl
 * @description # LinkPaymentCtrl Controller of the saasdmPhoenixApp
 */
.controller('LinkCtrl', function($scope, $rootScope, $sdp, $bank, $discount, $routeParams, $location, pdateFilter) {

	var status = {
		linkLoading : false,
		gateLoading: false,
		assetLoading: false		
	};
	
	function loadAsset(){
		var assetId = $routeParams.assetId ? $routeParams.assetId : null;
//		if(assetId === null){
//			return null;
//		}
		$scope.status.assetLoading = true;
		return $sdp.asset(assetId)//
		.then(function(asset) {
			$scope.asset = asset;
			$scope.status.assetLoading = false;
			return $scope.asset;
		});
	}
	
	function loadLink(){
		var linkSecureId = $routeParams.secureId ? $routeParams.secureId : null;
//		if(linkSecureId === null){
//			return null;
//		}
		$scope.status.linkLoading = true;
		return $sdp.link(linkSecureId)//
		.then(function(myLink) {
			return myLink;
		});
	}
		
	function chechActivation(myLink){
		return myLink.activate()//
		.then(function(link){
			$scope.status.linkLoading = false;
			$scope.link = link;
			$scope.translateParams.expirationDate = pdateFilter(link.expiry, 'jYYYY/jMM/jDD hh:mm');
		});
	}
	
	
	function loadGates(){
		$scope.status.gateLoading = true;
		return $bank.gates()//
		.then(function(gatesPag){
			$scope.gates = gatesPag.items;
			$scope.status.gateLoading = false;
			return gatesPag;
		});
	}
	
	function handleException(error){
		if(error.status === 401 && $rootScope.app.user.anonymous){
//			$amhNavigator.openDialog({
//				templateUrl: 'views/dialogs/amd-alert.html',
//				config: {
//					message: 'Login required'
//				}
//			});
			$scope.goTo('/users/login');
			return;
		}
		$scope.error = error.data.message;
		$scope.status.assetLoading = false;
		$scope.status.linkLoading = false;
		$scope.status.gateLoading = false;
		$scope.success = false;
	}

	
	function load() {

		// load link
		loadLink()
		// activate link
		.then(chechActivation)
		// load asset
		.then(loadAsset)		
		// load gates
		.then(loadGates)
		// Tell prerender.io to load page
		.then(function(){
			window.prerenderReady = true;
		})
		// handle exceptions
		.catch(handleException);
				
	}

	function pay(backend, discountCode){
		// create receipt and send to bank receipt page.
		var data = {
			'backend' : backend.id,
			'callback' : $location.absUrl()
		};
		if(typeof discountCode !== 'undefined' && discountCode !== null){
			data.discount_code = discountCode;
		}
		$scope.link.pay(data)//
		.then(function(receipt){
			$scope.goTo('/bank/receipts/' + receipt.id);
		}, handleException);
	}
	
	function checkDiscount(code){
		$discount.discount(code)//
		.then(function(discount){
			if(typeof discount.validation_code === 'undefined' || discount.validation_code === 0){
				$scope.discount_message = 'discount is valid';
			}else{
				switch(discount.validation_code){
				case 1:
					$scope.discount_message = 'discount is used before';
					break;
				case 2: 
					$scope.discount_message = 'discount is expired';
					break;
				case 3: 
					// discount is not owned by user.
					$scope.discount_message = 'discount is not valid';
					break;
				}
			}
		}, function(error){
			$scope.error = error.data.message;
			if(error.status === 404){				
				$scope.discount_message = 'discount is not found';
			}else{
				$scope.discount_message = 'unknown error while get discount info';
			}
		});
	}
	
	$scope.translateParams = {
		expirationDate : 'یک روز'
	};
	$scope.success = true;
	$scope.status = status;
	$scope.assetId = $routeParams.assetId;
	$scope.hostUrl = $location.protocol() + '://' + $location.host();
	$scope.pay = pay;
	$scope.checkDiscount = checkDiscount;

	load();
});
