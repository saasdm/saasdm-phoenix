/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @memberof saasdmPhoenixApp
 * @name ProfileCtrl
 * @description # ProfileCtrl
 * 
 */
.controller('ProfileCtrl', function($scope, $app) {

	var ctrl = {
		saveProfile : false,
		loadUser: false,
		loadProfile : false,
		user: null,
		profile: null,
//		educations: [
//			 'کارشناسی',
//			 'کارشناسی ارشد',
//			 'دکتری',
//			 'بالاتر'
//		],
//		familiar_mediums: [
//			 'جستجوی اینترنتی',
//			 'کانال‌ها و شبکه‌های اجتماعی',
//			 'معرفی دوستان',
//			 'مشاهده نمونه گزارش‌ها',
//			 'سایت‌های معرفی کننده',
//			 'تبلیغات',
//			 'نمایشگاه‌ها'
//		],
//		activity_fields:[
//			 'شرکت فناور',
//			 'پژوهشگر',
//			 'استاد دانشگاه',
//			 'دانشجو',
//			 'سرمایه‌گذار',
//			 'صنعت‌گر',
//			 'نهادهای دولتی',
//		],
//		favorite_items:{
//			'فناوری' : [
//				 'نانو فناوری',
//				 'مواد پیشرفته',
//				 'فناوری اطلاعات',
//				'الکترونیک',
//				'انرژی‌های نو',
//				'کامپوزیت و پلیمر',
//				'زیست فناوری'
//			],
//			'صنعت' : [
//				'خودرو',
//				'پوشش و خوردگی',
//				'نساجی',
//				'آب',
//				'دریایی',
//				'انرژی',
//				'صنایع غذایی',
//				'پلاستیک و پلیمر',
//				'نفت، گاز و پتروشیمی',
//				'ساختمان',
//				'سلامت و پزشکی',
//				'کشاورزی',
//				'بسته‌بندی',
//				'لوازم خانگی',
//				'هوافضا و نظامی',
//				'الکترونیک و کامپیوتر'
//			]
//		}
	};
	$scope.ctrl = ctrl;

	/**
	 * Sets a user in the scope
	 * 
	 * @param user
	 * @returns
	 */
	function setUser(user){
		ctrl.user = user;
		// TODO: set page title
	}

	/**
	 * Loads user data
	 * @returns
	 */
	function loadUser(){
		ctrl.loadUser = true;
		return $app.currentUser()//
		.then(function(user){
			setUser(user);
			ctrl.loadUser = false;
			return user;
		})//
		.then(function(us){
			ctrl.loadProfile = true;
			return us.profile()//
			.then(function(profile){
				ctrl.profile = profile;
				// activities
				if(!ctrl.profile.activities){
					ctrl.selectedActivities = [];
				}else{					
					ctrl.selectedActivities = ctrl.profile.activities.split(',');
				}
				// favorites
				if(!ctrl.profile.favorites){
					ctrl.selectedFavorites = [];
				}else{					
					ctrl.selectedFavorites = ctrl.profile.favorites.split(',');
				}
				ctrl.loadProfile = false;
				return profile;
			});
		})//
		.catch(function(){
			ctrl.loadUser = false;
			ctrl.loadProfile = false;
		});
	}

	/**
	 * Loads profile options. These settings are loaded from SPA config
	 */
	function loadProfileOptions(){
		$app.config('profileoptions')//
		.then(function(options){
			if(typeof options === 'undefined'){
				return;
			}
			ctrl.educations = options.educations;
			ctrl.familiar_mediums = options.familiar_mediums;
			ctrl.activity_fields = options.activity_fields;
			ctrl.favorite_items = options.favorite_items;
		});
	}
	
	/**
	 * Save current user
	 * 
	 * @returns
	 */
	function save(){
		ctrl.saveProfile = true;
		ctrl.profile.activities = ctrl.selectedActivities.join(',');
		ctrl.profile.favorites = ctrl.selectedFavorites.join(',');
		return ctrl.profile.update()//
		.then(function(){
			ctrl.saveProfile = false;
		})
		.catch(function(error){
			ctrl.error = error;
			ctrl.saveProfile = false;
		});
	}

	function toggle(item, list) {
		if(typeof list === 'undefined'){
			list = [];
		}
		var idx = list.indexOf(item);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item);
		}
	}

	function exists(item, list) {
		if(typeof list === 'undefined'){
			list = [];
		}
		return list.indexOf(item) > -1;
	}

	$scope.load = loadUser;
	$scope.save = save;
	$scope.toggle = toggle;
	$scope.exists = exists;

	loadUser();
	loadProfileOptions();

});
