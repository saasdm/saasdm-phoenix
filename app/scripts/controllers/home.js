/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:HomeCtrl
 * @memberof saasdmPhoenixApp
 * @description # HomeCtrl Controller of the saasdmPhoenixApp
 */
.controller('HomeCtrl', function($scope, $q, $location, $sdp, PaginatorParameter) {

	function loadNewestAssets() {
		// 
		var pp = new PaginatorParameter();
		pp.setFilter('type', 'file');
		pp.setSize(20);
		pp.setOrder('creation_dtime', 'd');
		// load assets
		return $sdp.assets(pp);
	}

	function loadMostDownloadAssets() {
		// prepare paginator parameter
		var pp = new PaginatorParameter();
		pp.setFilter('type', 'file');
		pp.setSize(20);
		pp.setOrder('download', 'd');
		// load assets
		return $sdp.assets(pp);
	}

	function load() {
		// init states
		$scope.ctrl.loadNewest = true;
		$scope.ctrl.loadMostDownload = true;
		// load newest assets
		var promise1 = loadNewestAssets().//
		then(function(assetList) {
			$scope.newest.items = assetList.items;
			$scope.ctrl.loadNewest = false;
		}, function(error) {
			alert('Fail to get assets: ' + error.data.message);
			$scope.newest.items = [];
			$scope.ctrl.loadNewest = false;
		});
		// load most downloaded assets
		var promise2 = loadMostDownloadAssets().//
		then(function(assetList) {
			$scope.mostDownload.items = assetList.items;
			$scope.ctrl.loadMostDownload = false;
		}, function(error) {
			alert('Fail to get assets: ' + error.data.message);
			$scope.mostDownload.items = [];
			$scope.ctrl.loadMostDownload = false;
		});
		
		$q.all([promise1, promise2])//
		.then(function(){
			window.prerenderReady = true;
		})//
		.catch(function(){
			window.prerenderReady = true;				
		});
	}

	$scope.newest = {
		items : []
	};
	$scope.mostDownload = {
		items : [],
		conf : {
			displayed : 5,
			perSlide : 1,
			delay : 3000
		}
	};
	$scope.ctrl = {
		loadNewest : false,
		loadMostDownload : false
	};
	$scope.load = load;
	$scope.reload = load;

	// loading page
	load();

});
