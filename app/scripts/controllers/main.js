/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')//

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:MainCtrl
 * @memberof saasdmPhoenixApp
 * @description # MainCtrl Controller of the saasdmPhoenixApp
 * 
 * کنترلر اصلی سیستم هست که کل نرم افزار را مدیریت می‌کند. این کنترل در
 * صفحه اصلی به کار گرفته شده و سایر بخش‌های سیستم به عنوان بخش‌هایی از
 * این کنترل در نظر گرفته می‌شود.
 * 
 * این کنترل در فایل اصلی سیستم استفاده می‌شه و تنها یکبار زمان اجرای
 * سیستم فراخوانی خواهد شد. استفاده از این کنترل در جاهای دیگه سیستم
 * منجر به بهم ریختگی سیستم خواهد شد.
 * 
 */
.controller('MainCtrl', function ($scope, $rootScope, $routeParams, $location, $cms, $usr, $sdp, $collection,
		$notify, $act, $translate, $mdDialog, $menu, $mdSidenav, $app, $page, $amhNavigator, appcache,
		PaginatorParameter) {

	// Check update
	appcache.checkUpdate().then(function () {
		appcache.swapCache();
		return confirm('app-update-message');
	});

//	/**
//	 * عمل ورود به سیستم را پیاده سازی می‌کند.
//	 */
//	function login(cridential) {
//		return $app.login(cridential)//
//		.then(function () {
//			$amhNavigator.openDialog({
//				templateUrl: 'views/dialogs/amd-alert.html',
//				config: {
//					message: 'login is successfull'
//				}
//			});
//		}, function () {
////			$notify.error({
////			title : 'login failed',
////			message : 'user name or passwrod is incorrect'
////			}, data);
//			$amhNavigator.openDialog({
//				templateUrl: 'views/dialogs/amd-alert.html',
//				config: {
//					message: 'user name or passwrod is incorrect'
//				}
//			});
//		});
//	}

//	/**
//	 * کاربر را از سیستم خارج می‌کند. با خارج شدن کاربر از سیستم
//	 * مسیر نرم‌افزار به صفحه اصلی انتقال پیدا می‌کند.
//	 */
//	function logout() {
//		return $app.logout()//
//		.then(function () {
//			$scope.ngEditable = false;
//			$amhNavigator.openDialog({
//				templateUrl: 'views/dialogs/amd-alert.html',
//				config: {
//					message: 'logout successfull'
//				}
//			});
//		}, function (data) {
//			$notify.error({
//				title : 'logout failed',
//				message : 'could not logout'
//			}, data);
//		});
//	}

	/**
	 * تعیین حالت کاربر جاری
	 * 
	 * این فراخوانی برای تعیین مدیر بودن کاربر جاری استفاده
	 * می‌شه. در صورتی که کاربر جاری یک کاربر دارای هویت باشده و
	 * دسترسی مدیریت رو داشته باشه، خروجی این فراخوانی درستی
	 * خواهد بود.
	 * 
	 * @return boolean مالک بودن کاربر جاری
	 */
	function isOwner() {
		// XXX: mason, 1395: تعیین مالک بودن کاربر جاری
		return $usr.isAdministrator();
	}

	function exec(command) {
		var args = Array.prototype.slice.call(arguments);
		args[0] = command.id;
		$act.execute.apply($act, args).then(function () {

		}, function (ex) {
			$notify.error('fail to execute command', ex);
		});
	}

	/**
	 * از این فراخوانی در تمام نرم افزار برای باز کردن منوها
	 * استفاده می‌شود.
	 * 
	 * @param {$mdOpenMenu}
	 *            $mdOpenMenu منوی معادل
	 * @param {Event}
	 *            ev رویداد سیستم
	 * @deprecated $scope.openMenu2
	 */
	function openMenu($mdOpenMenu, ev) {
		// originatorEv = ev;
		$mdOpenMenu(ev);
	}

	function openMenu2($mdMenu, ev) {
		// originatorEv = ev;
		$mdMenu.open(ev);
	}

	/**
	 * این تابع برای تغییر زبان سیستم به زبان مشخص استفاده
	 * می‌شود. کلید زبان مورد نظر باید در سیستم تعریف شده باشد.
	 * 
	 * @param {String}
	 *            key کلید زبان مورد نظر
	 * @return {promise} دستگیره برای اجرا
	 */
	function changeLanguage(key) {
		return $translate.use(key);
	}

	/**
	 * به صفحه دیگر با آدرس داده شده می‌رود
	 */
	function goTo(path) {
		$location.path(path);
	}

	$scope.isOwner = isOwner;
	$scope.exec = exec;
//	$scope.login = login;
//	$scope.logout = logout;
	$scope.openMenu = openMenu;
	$scope.openMenu2 = openMenu2;
	$scope.goTo = goTo;
	$scope.changeLanguage = changeLanguage;

	// ***********************************************
	// Methods for contents
	// ***********************************************

	function setSeoSetting(itemType, item){
		var title, description, keywords;
		switch (itemType) {
		case 'asset':
			title = item.name;
			description = item.description;
			break;
		case 'category':
			title = item.name;
			description = item.description;
			break;
		case 'content':
			title = item.label;
			description = item.description;
			keywords = item.keywords;
//			favicon = item.cover;
			break;
		case 'collection':
			title = item.title;
			description = item.description;
			break;
		}
		if(title !== undefined && title !== null && title !== ''){
			$page.setTitle(title);
		}
		if(description !== undefined && description !== null && description !== ''){
			$page.setDescription(description);
		}
		if(keywords !== undefined && keywords !== null && keywords !== ''){
			$page.setKeywords(keywords);
		}
//		if(favicon !== undefined)
//		$page.setFavicon(favicon);
	}

	function toggleEditable() {
		$scope.ngEditable = !$scope.ngEditable;
	}

	/**
	 * یک محتوا برای استفاده در این نمایش ایجاد می‌کند. این محتوی
	 * نامدار با استفاده از سرویس مدیریت محتوی ایجاد خواهد شد.
	 * 
	 * برای ایجاد محتوا یه پنجره به کاربر نشان می دهد تا برخی فیلدها را وارد کند
	 */
	function createNewContent() {
		var name = null;
		var data = {
				title : 'title',
				description : 'Content created from SaaSDM-Phoenix main controller',
				mime_type : 'application/weburger+json',
				file_name : $scope.name + '.json'
		};
		if(!$scope.ctrl.realPage){
			data.name = _getContentName();
		}
		// Update data by user
		$amhNavigator.openDialog({
			templateUrl : 'views/dialogs/amh-content.html',
			config : {
				data : data
			}
		})
		// Create content
		.then(function(contentData){
			name = contentData.name;
			contentData.name = contentData.name + '-' + $translate.use();
			return $cms.newContent(contentData);
		})//
		.then(function(newContent) {
			$scope.ctrl.status = 'ok';
			newContent.setValue({});
			return newContent;
		}, function() {
			alert('fail to create the content');
		});
	}

	function _createContent(contentName) {
		var data = {
				name : contentName,
				title : 'SDP content (' + contentName + ')',
				description : 'Content created from SaaSDM-Phoenix main controller.',
				mime_type : 'application/json',
				file_name : contentName + '.json'
		};
		return $cms.newContent(data)//
		.then(function (myContent) {
			$scope.content = myContent;
			$scope.content.setValue({});
			$scope.contentValue = {};
			return $scope.content;
		});
	}

	function _getContentName() {
		return ($routeParams.name || $rootScope.app.key);
	}

	function loadContentOfAsset(asset) {
		$scope.content = {};
		var myContent = asset.content ? asset.content
				: 'asset-content-' + asset.id;
		return $cms.content(myContent)//
		.then(function (nc) {
			return nc;
		}, function () {
			// Create content if not exist or error is occured
			return _createContent('asset-content-' + asset.id)//
			.then(function (nc) {
				asset.content = nc.id;
				asset.update();
				return nc;
			}, function () {
				// do nothing
			});
		})//
		.then(function (nc) {
			if (typeof nc !== 'undefined') {
				$scope.ctrl.realPage = true;
				$scope.content = nc;
				nc.value()
				.then(function (val) {
					if(val === null || val === ''){
						$scope.content.setValue({});
						$scope.contentValue = {};
					}else{                		
						$scope.contentValue = val;
					}
				});
			}
		});
	}

	function loadContentOfCategory(category) {
		$scope.content = {};
		var myContent = category.content ? category.content
				: 'category-content-' + category.id;
		return $cms.content(myContent)//
		.then(function (nc) {
			return nc;
		}, function () {
			return _createContent('category-content-' + category.id)//
			.then(function (nc) {
				category.content = nc.id;
				category.update();
				return nc;
			}, function () {
				// do nothing
			});
		})//
		.then(function (nc) {
			if (typeof nc !== 'undefined') {
				$scope.ctrl.realPage = true;
				$scope.content = nc;
				nc.value()
				.then(function (val) {
					if(val === null || val === ''){
						$scope.content.setValue({});
						$scope.contentValue = {};
					}else{
						$scope.contentValue = val;
					}
				});
			}
		});
	}

	/**
	 * Save or update content
	 * @returns
	 */
	function saveContent() {
		return $scope.content.setValue($scope.contentValue)//
		.then(function() {
			alert('content is saved successfully');
		}, function(){
			alert('saving content is failed');
		});
	}

	$scope.ngEditable = false;
	$scope.toggleEditable = toggleEditable;
	$scope.setSeoSetting = setSeoSetting;
	$scope.content = null;
	$scope.contentValue = null;
	$scope.createNewContent = createNewContent;
	$scope.saveContent = saveContent;
	$scope.loadContentOfAsset = loadContentOfAsset;
	$scope.loadContentOfCategory = loadContentOfCategory;

	// ***********************************************
	// Methods for assets, categories and collections
	// ***********************************************

	function find(query) {
		if (query) {
			$location.path('/assets/' + query);
		} else {
			$location.path('/assets');
		}
	}

	function loadRootItems() {
		$scope.ctrl.loadRoot = true;
		// prepare paginator parameter
		var pp = new PaginatorParameter();
		pp.setFilter('parent', '0');
		pp.setSize(20);
		pp.setOrder('id', 'd');
		// load assets
		return $sdp.assets(pp)//
		.then(function (rootItems) {
			$scope.rootItems = rootItems.items;
		}, function (error) {
			alert('Fail to get root items: ' + error.data.message);
			$scope.rootItems = [];
			$scope.ctrl.loadRoot = false;
		});
	}

	function loadRootCategories() {
		$scope.ctrl.loadRoot = true;
		// prepare paginator parameter
		var pp = new PaginatorParameter();
		pp.setFilter('parent', '0');
		pp.setSize(20);
		pp.setOrder('id', 'd');
		// load assets
		return $sdp.categories(pp)//
		.then(function (rootCats) {
			$scope.rootCategories = rootCats.items;
		}, function (error) {
			alert('Fail to get root items: ' + error.data.message);
			$scope.rootCategories = [];
			$scope.ctrl.loadRoot = false;
		});
	}

	function loadCollections() {
		$scope.ctrl.loadEvents = true;
		// load event menu collection
		return $collection.collection('__event_menu_collection__')//
		.then(function(collection){
			// prepare paginator parameter
			var pp = new PaginatorParameter();
			pp.setSize(20);
			pp.setOrder('id', 'a');
			return collection.documents(pp)//
			.then(function (events){
				return events;
			});
		})
//		return $collection.collections(pp)//
		.then(function (eventsPag) {
			$scope.events = eventsPag.items;
			var events = {
					title : 'events',
					url : '#!/events',
					items : []
			};
			var i;
			for(i=0 ; i<$scope.events.length ; i++){
				var ev = $scope.events[i];
				events.items.push({
					title : ev.title,
					url : '#!/events/' + ev.collection_name
				});
			}
			$menu.addItem('dropdown', events);
			$scope.ctrl.loadEvents = false;
		})//
		.catch(function () {
//			alert('Fail to get event items: ' + error.data.message);
			$scope.events = [];
			$scope.ctrl.loadEvents = false;
		});
	}

	function loadHirarchyChain(child, chainArray) {
		if (!child.hasParent()) {
			return;
		}
		return child.getParent().//
		then(function (parent) {
			chainArray.unshift(parent);
			return loadHirarchyChain(parent, chainArray);
		});
	}

	function load() {
		// load root items
		loadRootItems();
		// load root categories
		loadRootCategories();
		// load collections
		loadCollections();
	}

	$scope.rootItems = [];
	$scope.rootCategories = [];
	$scope.collections = [];
	$scope.query = '';
	$scope.search = find;
	$scope.loadHirarchyChain = loadHirarchyChain;

	$scope.ctrl = {
			loadRoot : false,
			realPage: false
	};

	// Load data
	load();

	// ***********************************************

	var actionLogin = {
			label : 'sign in',
			description : 'user login action',
			helpId : 'user.login',
			icon : 'input',
			priority : 3,
			visible : function () {
				return $rootScope.app.user.anonymous;
			},
			action : function () {
//				return $mdDialog.show({
//				controller : 'sdpDialogsCtrl',
//				templateUrl : 'views/dialogs/login.html',
//				parent : angular.element(document.body),
//				clickOutsideToClose : true,
//				locals : {
//				model : {},
//				style : {
//				title : 'service'
//				}
//				}
//				}).then(function (c) {
//				return $scope.login(c);
//				});
				$amhNavigator.openView('/users/login');
			}
	};

	var actionAccount = {
			label : 'user area',
			description : 'go to user area',
			helpId : 'user',
			icon : 'account_circle',
			priority : 3,
			visible : function () {
				return !$rootScope.app.user.anonymous;
			},
			action : function () {
				$amhNavigator.openView('/users/user-area');
			}
	};

	var actionLogout = {
			label : 'sign out',
			description : 'user logout action',
			helpId : 'user',
			icon : 'power_settings_new',
			priority : 3,
			visible : function () {
				return !$rootScope.app.user.anonymous;
			},
			action : function () {
				$scope.logout()//
				.then(function(){					
					$scope.goTo('/users/login');
				});
			}
	};

	// *********************************************
	// Create main menu items
	// *********************************************

	function _getLanguage(){
		return $scope.app.setting.language || 
		$scope.app.config.language ||
		'fa';
	}

	function createUrlLink(label, description, icon, priority,
			url) {
		return {
			label : label,
			description : description,
			icon : icon,
			visible : function () {
				return true;
			},
			priority : priority,
			link : url
		};
	}

	var actionHome = createUrlLink('home', '', 'home', 3,
	'/#!/');
	var actionAbout = createUrlLink('about us', '', 'help', 3,
	'#!/content/about-us-' + _getLanguage());
	var actionContact = createUrlLink('contact us', '',
			'contact_phone', 3, '#!/content/contact-us-' + _getLanguage());
	var actionSpecialService = createUrlLink(
			'special services', '', 'gift', 3,
	'#!/content/special-services-' + _getLanguage());
	var actionTermsOfService = createUrlLink(
			'terms of service', '', 'chat', 3,
	'#!/content/terms-of-service-' + _getLanguage());
	var actionFaq = createUrlLink(
			'help and faq', '', 'help', 3,
	'#!/content/help-and-faq-' + _getLanguage());
	var actionDashboard = createUrlLink('management panel', '',
			'adjust', 3, '/saasdm-cpanel/#!/');
	actionDashboard.visible = function () {
		return $rootScope.app.user.owner;
	};

	$menu//
	.addItem('rhmenu', actionHome)//
	.addItem('rhmenu', actionAbout)//
	.addItem('rhmenu', actionContact)//
	.addItem('rhmenu', actionSpecialService)//
	.addItem('rhmenu', actionLogin)//
	.addItem('rhmenu', actionAccount)//
	.addItem('rhmenu', actionLogout);

	$menu//
	.addItem('rfmenu', actionDashboard)//
	.addItem('rfmenu', actionFaq)//
	.addItem('rfmenu', actionTermsOfService)//
	.addItem('rfmenu', actionContact)//
	.addItem('rfmenu', actionAbout);

	$menu//
	.addItem('sidebar', actionLogin)//
	.addItem('sidebar', actionLogout)//
	.addItem('sidebar', actionAccount)//
	.addItem('sidebar', actionContact)//
	.addItem('sidebar', actionAbout)//
	.addItem('sidebar', actionHome);

	function loadSocialLinks(){
		$app.config('sociallinks')//
		.then(function(links){
			if(!angular.isDefined(links)){
				return;
			}
			var link = null;
			if(links.facebook){
				link = createUrlLink('facebook', '', 'facebook', 3, links.facebook);
				$menu.addItem('social', link);
			}
			if(links.instagram){
				// 'https://www.instagram.com/hitechmonitor/'
				link = createUrlLink('instagram', '', 'wb-social-instagram', 3, links.instagram);
				$menu.addItem('social', link);
			}
			if(links.googleplus){
				link = createUrlLink('g+', '', 'wb-social-googleplus', 3, links.googleplus);
				$menu.addItem('social', link);
			}
			if(links.linkedin){
				// 'https://www.linkedin.com/company/13332484/'
				link = createUrlLink('linkedin', '', 'linkedin', 3, links.linkedin);
				$menu.addItem('social', link);
			}
			if(links.twitter){		
				link = createUrlLink('twitter', '', 'twitter', 3, links.twitter);
				$menu.addItem('social', link);
			}
			if(links.telegram){
				// 'https://t.me/hitechmonitor'
				link = createUrlLink('telegram', '', 'wb-social-telegram', 3, links.telegram);
				$menu.addItem('social', link);
			}
		});
	}

	loadSocialLinks();

	/*
	 * تمام منوهای سیستم بارگذاری می‌شود
	 */
	$scope.lhmenue = $menu.menu('lhmenu');
	$scope.rhmenu = $menu.menu('rhmenu');
	$scope.lfmenu = $menu.menu('lfmenu');
	$scope.rfmenu = $menu.menu('rfmenu');
	$scope.dropdown = $menu.menu('dropdown');
	$scope.sidebar = $menu.menu('sidebar');
	$scope.social = $menu.menu('social');

	// *********************************************

	$scope.languages = [ {
		title : 'persian',
		key : 'fa',
	} ];

	$rootScope.toggleSidenav = function (id) {
		$mdSidenav(id).toggle();
	};

});
