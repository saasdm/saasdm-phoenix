/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:SaaSDMPhoenixFooterCtrl
 * @description # SaaSDMPhoenixFooterCtrl Controller of the saasdmPhoenixApp
 */
.controller('SaaSDMPhoenixFooterCtrl', function($scope, $rootScope, $app, $menu) {

	$scope.lfmenu = $menu.menu('lfmenu');
	$scope.rfmenu = $menu.menu('rfmenu');
	$scope.social = $menu.menu('social');
	if($rootScope.app.config.brand && $rootScope.app.config.brand.title){		
		$scope.translateParams = {
			copyrightOwner : $rootScope.app.config.brand.title
		};
	}
});
