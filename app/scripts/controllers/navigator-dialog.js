'use strict';
angular.module('saasdmPhoenixApp')

/**
 * @ngdoc function
 * @name saasdmPhoenixApp.controller:AmdNavigatorDialogCtrl
 * @description # AmdNavigatorDialogCtrl Controller of the saasdmPhoenixApp
 */
.controller('AmdNavigatorDialogCtrl', function($scope, $mdDialog, config) {
    $scope.config = config;
    $scope.hide = function() {
	$mdDialog.hide();
    };
    $scope.cancel = function() {
	$mdDialog.cancel();
    };
    $scope.answer = function(a) {
	$mdDialog.hide(a);
    };
});
