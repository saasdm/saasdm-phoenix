/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:AssetsCtrl
 * @description # AssetsCtrl Controller of the saasdmPhoenixApp
 * 
 * این کنترلر وظیفه مدیریت فهرستی از دارایی‌ها را بر عهده دارد. این
 * کنترلر را می‌توان با ارسال یک پارامتر با نام query یا بدون ارسال
 * پارامتر فراخوانی کرد. در صورتی که پارامتر query به این کنترلر ارسال
 * شود فهرست کل دارایی‌هایی که در کوئری داده شده صدق می‌کنند نمایش داده
 * می‌شود و در این جستجو دارایی‌های تو در تو نیز در نظر گرفته می‌شوند.
 * در صورتی که این کنترلر بدون پارامتر فراخوانی شود یعنی پارامتر query
 * ارسال نشود در این صورت این کنترلر دارایی‌های سطح اول یعنی دارایی‌هایی
 * که فرزند هیچ دارایی دیگری نیستند را نمایش می‌دهد.
 * 
 */
.controller('AssetsCtrl', function($scope, $sdp, $routeParams, PaginatorParameter) {

	var lastPage = null;
	var searchParam = {
		searchWord : '',
		filterBy : '',
		filterValue : '',
		sortBy : '',
		sortType : 'd'
	};
	var query = $routeParams.query ? decodeURIComponent($routeParams.query)	: null;

	$scope.assets = [];
	$scope.pp = new PaginatorParameter();
	$scope.pp.setFilter('parent', 0);
	$scope.pp.setOrder('id', 'd');
	$scope.working = false;

	function nextPage() {
		if ($scope.working) {
			return;
		}
		if (lastPage && !lastPage.hasMore()) {
			return;
		}
		if (lastPage) {
			$scope.pp.setPage(lastPage.next());
		} else {
			$scope.pp.setPage(0);
		}
		$scope.working = true;

		// find assets
		$sdp.assets($scope.pp)//
		.then(function(assetList) {
			$scope.assets = $scope.assets.concat(assetList.items);
			$scope.working = false;
			lastPage = assetList;
			return assetList;
		}, function(error) {
			print('Fail to get assets: ' + error.data.message);
			$scope.working = false;
		})//
		.finally(function() {
			window.prerenderReady = true;
		});
	}

	function setAsset(id) {
		$scope.asset = $sdp.asset(id);
	}

	/**
	 * جستجوی دارایی‌ها
	 * 
	 * @param query
	 * @returns
	 */
	function find(query) {
		$scope.pp.setQuery(query);
		$scope.pp.setFilter();
		reload();
	}

	function reload() {
		$scope.assets = [];
		lastPage = null;
		nextPage();
	}

	function advancedSearch() {
		// $scope.pp = new PaginatorParameter();
		if ($scope.searchParam.filterBy && $scope.searchParam.filterValue) {
			$scope.pp.setFilter($scope.searchParam.filterBy,
					$scope.searchParam.filterValue);
		} else {
			$scope.pp.setFilter('parent', 0);
		}
		if ($scope.searchParam.sortBy && $scope.searchParam.sortType) {
			$scope.pp.setOrder($scope.searchParam.sortBy,
					$scope.searchParam.sortType);
		} else {
			$scope.pp.setOrder();
		}
		if ($scope.searchParam.searchWord) {
			$scope.pp.setQuery($scope.searchParam.searchWord);
		} else {
			$scope.pp.setQuery();
		}
		reload();
	}

	$scope.content = null;
	$scope.searchParam = searchParam;
	$scope.nextPage = nextPage;
	$scope.setAsset = setAsset;
	$scope.search = find;
	$scope.advancedSearch = advancedSearch;

	if (query) {
		find(query);
	} else {
		nextPage();
	}

});
