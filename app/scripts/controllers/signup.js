/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:SignupCtrl
 * @memberof saasdmPhoenixApp
 * @description # SignupCtrl Controller of the saasdmPhoenixApp
 */
.controller('SignupCtrl', function($scope, $usr, $app, $window) {

	var ctrl = {
		createUser: false,
		loadUser: false,
		saveProfile : false,
		loadProfile : false,
//		educations: [
//			 'کارشناسی',
//			 'کارشناسی ارشد',
//			 'دکتری',
//			 'بالاتر'
//		],
//		familiar_mediums: [
//			 'جستجوی اینترنتی',
//			 'کانال‌ها و شبکه‌های اجتماعی',
//			 'معرفی دوستان',
//			 'مشاهده نمونه گزارش‌ها',
//			 'سایت‌های معرفی کننده',
//			 'تبلیغات',
//			 'نمایشگاه‌ها'
//		],
//		activity_fields:[
//			 'شرکت فناور',
//			 'پژوهشگر',
//			 'استاد دانشگاه',
//			 'دانشجو',
//			 'سرمایه‌گذار',
//			 'صنعت‌گر',
//			 'نهادهای دولتی',
//		],
//		favorite_items:{
//			'فناوری' : [
//				 'نانو فناوری',
//				 'مواد پیشرفته',
//				 'فناوری اطلاعات',
//				'الکترونیک',
//				'انرژی‌های نو',
//				'کامپوزیت و پلیمر',
//				'زیست فناوری'
//			],
//			'صنعت' : [
//				'خودرو',
//				'پوشش و خوردگی',
//				'نساجی',
//				'آب',
//				'دریایی',
//				'انرژی',
//				'صنایع غذایی',
//				'پلاستیک و پلیمر',
//				'نفت، گاز و پتروشیمی',
//				'ساختمان',
//				'سلامت و پزشکی',
//				'کشاورزی',
//				'بسته‌بندی',
//				'لوازم خانگی',
//				'هوافضا و نظامی',
//				'الکترونیک و کامپیوتر'
//			]
//		},
		selectedActivities : [],
		selectedFavorites : []
	};
	$scope.ctrl = ctrl;
	
	/**
	 * عمل ثبت‌نام کاربر در سیستم را پیاده سازی می‌کند.
	 */
	function signup(data) {
		ctrl.createUser = true;
		$scope.signupMessage = '';
		data.login = data.email;
		return $usr.newUser(data)//
		.then(function () {
			ctrl.createUser = false;
			$scope.signupMessage = 'signup is successful';
			$app.login(data)//
			.then(function () {
				createUserProfile(data.profile);
			});
//			$scope.goTo('/');
		}, function (error) {
			ctrl.createUser = false;
			if (error.status === 400) {
				$scope.signupMessage = 'username is existed';
			}else{
				$scope.signupMessage = 'signup is failed';				
			}
		});
	}

	function createUserProfile(data) {
		ctrl.saveProfile = true;
		return $app.currentUser()//
		.then(function (usr) {
			return usr;
		})//
		.then(function(user){
			return user.profile()//
			.then(function (profile) {
				return profile;
			});
		})//
		.then(function(profile){
			data.activities = ctrl.selectedActivities.join(',');
			data.favorites = ctrl.selectedFavorites.join(',');
			profile.setData(data);
			return profile.update()//
			.then(function(){
				ctrl.saveProfile = false;
				$scope.signupMessage = 'profile is created';
			});
		})//
		.catch(function () {
			ctrl.saveProfile = false;
			$scope.signupMessage = 'create profile is failed';
		});
	}

	/**
	 * Loads profile options. These settings are loaded from SPA config
	 */
	function loadProfileOptions(){
		$app.config('profileoptions')//
		.then(function(options){
			if(typeof options === 'undefined'){
				return;
			}
			ctrl.educations = options.educations;
			ctrl.familiar_mediums = options.familiar_mediums;
			ctrl.activity_fields = options.activity_fields;
			ctrl.favorite_items = options.favorite_items;
		})//
		.finally(function() {
			window.prerenderReady = true;
		});
	}
	
//	/**
//	 * Save profile
//	 * 
//	 * @returns
//	 */
//	function saveProfile(profile){
//		ctrl.saveProfile = true;
//		profile.activities = ctrl.selectedActivities.join(',');
//		profile.favorites = ctrl.selectedFavorites.join(',');
//		return profile.update()//
//		.then(function(){
//			ctrl.saveProfile = false;
//			$scope.signupMessage = 'profile is created';
//		})
//		.catch(function(error){
//			ctrl.error = error;
//			ctrl.saveProfile = false;
//			$scope.signupMessage = 'create profile is failed';
//		});
//	}
	
	function toggle(item, list) {
		if(typeof list === 'undefined'){
			list = [];
		}
		var idx = list.indexOf(item);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item);
		}
	}

	function exists(item, list) {
		if(typeof list === 'undefined'){
			list = [];
		}
		return list.indexOf(item) > -1;
	}
	
	$scope.back = function() {
		$window.history.back();
	};
	
	$scope.signup = signup;
	$scope.toggle = toggle;
	$scope.exists = exists;
	
	loadProfileOptions();
	
});
