/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:CategoryCtrl
 * @description # CategoryCtrl Controller of the saasdmPhoenixApp
 * 
 */
.controller('CategoryCtrl',	function($scope, $q, $sdp, $routeParams, $app, PaginatorParameter) {


	function loadSubcategories() {
		var ppc = new PaginatorParameter();
		ppc.setFilter('parent', $routeParams.categoryId);
		$scope.fetchingCategories = true;
		$sdp.categories(ppc)//
		.then(function(subCats) {
			$scope.categories = $scope.categories
			.concat(subCats.items);
			$scope.fetchingCategories = false;
		},
		function(error) {
			print('Fail to get categories: ' + error.data.message);
			$scope.fetchingCategories = false;
		});
	}

	function nextPage() {
		if ($scope.working) {
			return;
		}
		if (lastPage && !lastPage.hasMore()) {
			return;
		}
		if (lastPage) {
			$scope.pp.setPage(lastPage.next());
		} else {
			$scope.pp.setPage(1);
		}
		$scope.working = true;

		// find assets
		$scope.category.assets($scope.pp)//
		.then(function(assetList) {
			$scope.assets = $scope.assets.concat(assetList.items);
			$scope.working = false;
			lastPage = assetList;
			return assetList;
		}, function(error) {
			print('Fail to get assets: ' + error.data.message);
			$scope.working = false;
		});
	}

	/**
	 * جستجوی دارایی‌ها
	 * 
	 * @param query
	 * @returns
	 */
	function find(query) {
		$scope.pp.setQuery(query);
		reload();
	}

	function reload() {
		$scope.assets = [];
		lastPage = null;
		nextPage();
	}

	function load() {
		$scope.content = {};
		var categoryId;
		if ($routeParams.categoryId) {
			categoryId = $routeParams.categoryId;
		} else {
			// error
			return;
		}

		$scope.working = true;

		return $sdp.category(categoryId)//
		.then(function(category) {
			$scope.category = category;
			$scope.working = false;
			$scope.error = 0;
			return $scope.category;
		}, function(error) {
			console.log('Fail to get category: ' + error.data.message);
			$scope.error = error.status;
			$scope.working = false;
		})//
		.then(function(category) {
			if (typeof category === 'undefined'){
				return;
			}
			$scope.setSeoSetting('category', $scope.category);
			// load hirarchical chain of asset
			var promise1 = $scope.loadHirarchyChain(category, $scope.chain);
			var promise2 = $scope.loadContentOfCategory(category);
			
			$q.all([promise1, promise2])//
			.then(function(){
				window.prerenderReady = true;
			})//
			.catch(function() {
				window.prerenderReady = true;
			});
			
		})//
		.then(function() {
			loadSubcategories();
			nextPage();
		});
	}

	function advancedSearch() {
		// $scope.pp = new PaginatorParameter();
		if ($scope.searchParam.filterBy && $scope.searchParam.filterValue) {
			$scope.pp.setFilter($scope.searchParam.filterBy,
					$scope.searchParam.filterValue);
		} else {
			$scope.pp.setFilter(null, null);
		}
		if ($scope.searchParam.sortBy && $scope.searchParam.sortType) {
			$scope.pp.setOrder($scope.searchParam.sortBy,
					$scope.searchParam.sortType);
		} else {
			$scope.pp.setOrder();
		}
		if ($scope.searchParam.searchWord) {
			$scope.pp.setQuery($scope.searchParam.searchWord);
		} else {
			$scope.pp.setQuery();
		}
		reload();
	}

	var lastPage = null;
	var searchParam = {
			searchWord : '',
			filterBy : '',
			filterValue : '',
			sortBy : '',
			sortType : 'd'
	};

	$app
	.scopeMenu($scope)//
	.add({ // edit menu
		priority : 15,
		icon : 'edit',
		label: 'Edit content',
		visible : function() {
			return $scope.app.user.owner;
		},
		action : $scope.toggleEditable
	})//
	.add({ // add new page
		priority : 15,
		icon : 'add_box',
		label: 'Create new content',
		visible : function() {
			return $scope.app.user.owner;
		},
		action : $scope.createNewContent
	})//
	.add({ // save menu
		priority : 10,
		icon : 'save',
		label: 'Save current changes',
		visible : function() {
			return $scope.app.user.owner;
		},
		action : $scope.saveContent
	});

	$scope.content = {};
	$scope.categoryId = $routeParams.categoryId;
	$scope.error = 0;
	$scope.assets = [];
	$scope.categories = [];
	$scope.chain = [];
	$scope.pp = new PaginatorParameter();
	$scope.pp.setOrder('id', 'd');
	$scope.working = false;
	$scope.fetchingCategories = false;

	$scope.searchParam = searchParam;
	$scope.load = load;
	$scope.search = find;
	$scope.nextPage = nextPage;
	$scope.advancedSearch = advancedSearch;

	load();

	$scope.$watch('contentValue', function(){
		if($scope.contentValue !== undefined && $scope.contentValue !== null){
			$scope.setSeoSetting('content', $scope.contentValue);
		}
	}, true);
});
