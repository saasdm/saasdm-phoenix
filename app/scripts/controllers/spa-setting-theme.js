///* jslint todo: true */
///* jslint xxx: true */
///* jshint -W100 */
//'use strict';
//
//angular.module('saasdmPhoenixApp')
//
///**
// * @ngdoc controller
// * @name saasdmPhoenixApp.controller:SpaSettingThemeCtrl
// * @description Define our default color generator controller!
// */
//.controller('SpaSettingThemeCtrl', function ($scope, $mdTheming, $app, $rootScope, themeProvider){
//	
//	$scope.ctrl = {
//		primary: null,
//		warn: null,
//		accent: null,
//		background: null
//	};
//
//	$app.config('themepalettes')//
//	.then(function(options){
//		$scope.ctrl = options;
//		if(typeof options === 'undefined'){
//			$rootScope.app.config.themepalettes = {};
//		}
////		if(typeof $scope.ctrl === 'undefined'){
////		return;
////		}
////		setPrimaryPalette($scope.ctrl.primary);
////		setAccentPalette($scope.ctrl.accent);
////		setWarnPalette($scope.ctrl.warn);
////		setBackgroundPalette($scope.ctrl.background);
//	});
//
//	function setPrimaryPalette(paletteStr){
//		if(paletteStr){			
//			var primaryPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customPrimary', primaryPalette);
//			themeProvider.theme('default').primaryPalette('customPrimary');
//		}else{
//			// Set default primary palette of AngularJS Material
//			themeProvider.theme('default').primaryPalette('indigo');		
//		}
//	}
//
//	function setAccentPalette(paletteStr){
//		if(paletteStr){			
//			var accentPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customAccent', accentPalette);
//			themeProvider.theme('default').accentPalette('customAccent');
//		}else{
//			// Set default accent palette of AngularJS Material
//			themeProvider.theme('default').accentPalette('pink');			
//		}
//	}
//
//	function setWarnPalette(paletteStr){		
//		if(paletteStr){			
//			var warnPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customWarn', warnPalette);
//			themeProvider.theme('default').warnPalette('customWarn');
//		}else{
//			// Set default warn palette of AngularJS Material
//			themeProvider.theme('default').warnPalette('red');			
//		}
//	}
//
//	function setBackgroundPalette(paletteStr){
//		if(paletteStr){			
//			var backgroundPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customBackground', backgroundPalette);
//			themeProvider.theme('default').backgroundPalette('customBackground');
//		}else{
//			// Set default background palette of AngularJS Material
//			themeProvider.theme('default').backgroundPalette('grey');
//		}
//	}
//
//	function saveTheme() {
//		//create new theme
//		setPrimaryPalette($scope.ctrl.primary);
//		setAccentPalette($scope.ctrl.accent);
//		setWarnPalette($scope.ctrl.warn);
//		setBackgroundPalette($scope.ctrl.background);
//		
//		//reload the theme
//		$mdTheming.generateTheme('default');
//
//		//optional - set the default to this new theme
//		themeProvider.setDefaultTheme('default');
//		
//		if($scope.ctrl.primary){
//			$rootScope.app.config.themepalettes.primary = $scope.ctrl.primary;
//		}
//		if($scope.ctrl.accent){
//			$rootScope.app.config.themepalettes.accent = $scope.ctrl.accent;
//		}
//		if($scope.ctrl.warn){
//			$rootScope.app.config.themepalettes.warn = $scope.ctrl.warn;
//		}
//		if($scope.ctrl.bachground){
//			$rootScope.app.config.themepalettes.bachground = $scope.ctrl.bachground;
//		}
//	}
//
//	$scope.save = saveTheme;
//
////	$scope.$watch('ctrl.primary', setPrimaryPalette, true);	
////	$scope.$watch('ctrl.accent', setAccentPalette, true);	
////	$scope.$watch('ctrl.warn', setWarnPalette, true);	
////	$scope.$watch('ctrl.background', setBackgroundPalette, true);	
//
//});
//
//
