/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc controller
 * @name saasdmPhoenixApp.controller:FolderCtrl
 * @description # FolderCtrl Controller of the saasdmPhoenixApp
 */
.controller('FolderCtrl', function($scope, $sdp, $routeParams, PaginatorParameter) {

	function nextPage() {
		if ($scope.working) {
			return;
		}
		if (lastPage && !lastPage.hasMore()) {
			return;
		}
		if (lastPage) {
			$scope.pp.setPage(lastPage.next());
		} else {
			$scope.pp.setPage(1);
		}
		$scope.working = true;

		// find assets
		$sdp.assets($scope.pp)//
		.then(function(assetList) {
			$scope.assets = $scope.assets.concat(assetList.items);
			$scope.working = false;
			lastPage = assetList;
			return assetList;
		}, function(error) {
			alert('Fail to get assets: ' + error.data.message);
			$scope.working = false;
		})//
		.finally(function() {
			window.prerenderReady = true;
		});
	}

	/**
	 * جستجوی دارایی‌ها
	 * 
	 * @param query
	 * @returns
	 */
	function find(query) {
		$scope.pp.setQuery(query);
		reload();
	}

	function reload() {
		$scope.assets = [];
		lastPage = null;
		nextPage();
	}

	function load() {
		var folderId;
		if ($routeParams.folderId) {
			folderId = $routeParams.folderId;
		} else {
			// error
			return;
		}

		$scope.working = true;

		return $sdp.asset(folderId)//
		.then(function(asset) {
			$scope.asset = asset;
			// load hirarchical chain of asset
			$scope.loadHirarchyChain(asset, $scope.chain);
			$scope.working = false;
			return $scope.asset;
		}, function(error) {
			print('Fail to get asset: ' + error.data.message);
			$scope.working = false;
		})//
		.then(function(asset) {
			if(asset === undefined){
				return;
			}
			$scope.setSeoSetting('asset', $scope.asset);
			$scope.loadContentOfAsset(asset);
		})//
		.then(function() {
			nextPage();
		});
	}

	function advancedSearch() {
		// $scope.pp = new PaginatorParameter();
		if ($scope.searchParam.filterBy && $scope.searchParam.filterValue) {
			$scope.pp.setFilter($scope.searchParam.filterBy,
					$scope.searchParam.filterValue);
		} else {
			$scope.pp.setFilter('parent', 0);
		}
		if ($scope.searchParam.sortBy && $scope.searchParam.sortType) {
			$scope.pp.setOrder($scope.searchParam.sortBy,
					$scope.searchParam.sortType);
		} else {
			$scope.pp.setOrder();
		}
		if ($scope.searchParam.searchWord) {
			$scope.pp.setQuery($scope.searchParam.searchWord);
		} else {
			$scope.pp.setQuery();
		}
		reload();
	}

	var lastPage = null;
	var searchParam = {
		searchWord : '',
		filterBy : '',
		filterValue : '',
		sortBy : '',
		sortType : 'd'
	};

	$scope.assets = [];
	$scope.chain = [];
	$scope.pp = new PaginatorParameter();
	$scope.pp.setFilter('parent', $routeParams.folderId);
	$scope.pp.setOrder('id', 'a');
	$scope.working = false;
	
	$scope.searchParam = searchParam;
	$scope.folderId = $routeParams.folderId;
	$scope.load = load;
	$scope.search = find;
	$scope.nextPage = nextPage;
	$scope.advancedSearch = advancedSearch;

	load();
	
	$scope.$watch('contentValue', function(){
		if($scope.contentValue !== undefined && $scope.contentValue !== null){
			$scope.setSeoSetting('content', $scope.contentValue);
		}
	}, true);
});
