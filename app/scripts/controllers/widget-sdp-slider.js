/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('saasdmPhoenixApp')
/**
 * 
 */
.controller('SdpAssetSliderCtrl', function($scope, $sdp, PaginatorParameter) {
	var ngModel = $scope.wbModel;
	$scope.ctrl = {
		loadingAssets: false,
		assets: null
	};

	function loadAssets(){
		$scope.ctrl.loadingAssets = true;
		var pp = new PaginatorParameter();
		pp.setFilter('type', 'file');
		pp.setSize(ngModel.query.count);
		pp.setOrder(ngModel.query.sortKey, ngModel.query.sortOrder);
		var tempId = ngModel.query.includeTag;
		if(typeof tempId !== 'undefined' && tempId !== 0){
			pp.put('include_tag', tempId);
		}
		tempId = ngModel.query.excludeTag;
		if(typeof tempId !== 'undefined' && tempId !== 0){
			pp.put('exclude_tag', tempId);
		}
		tempId = ngModel.query.includeCategory;
		if(typeof tempId !== 'undefined' && tempId !== 0){
			pp.put('include_category', tempId);
		}
		tempId = ngModel.query.excludeCategory;
		if(typeof tempId !== 'undefined' && tempId !== 0){
			pp.put('exclude_category', tempId);
		}		
		// load assets
		return $sdp.assets(pp)//
		.then(function (assetList) {
			$scope.ctrl.assets = assetList;
			return assetList;
		}, function (error) {
			console.log('Fail to get assets', error);
			$scope.ctrl.assets = null;
		})//
		.finally(function(){
			$scope.ctrl.loadingAssets = false;
		});
	}
	
	/*
	 * Listen model
	 */
	$scope.$watch('wbModel',  function() {
		if (angular.isDefined($scope.wbModel)) {
			ngModel = $scope.wbModel;
			if (!angular.isDefined(ngModel.query)) {
				ngModel.query = {
					count: 20,
					includeTag: 0,
					excludeTag: 0,
					includeCategory: 0,
					excludeCategory: 0,
					sortKey: 'id',
					sortOrder: 'a',
					filterKey: '',
					filterValue: ''
				};
			}
			if(!angular.isDefined(ngModel.slider)){
				ngModel.slider = {
					displayed: 6,
					perSlide: 6,
					displayedXs: 1,
					perSlideXs: 1,
					displayedSm: 3,
					perSlideSm: 3,
					displayedMd: 4,
					perSlideMd: 4,
					animation: 'slide',
					direction: 'horizontal',
					timeInterval: 2000,
					transitionDuration: 800,
					autoplay: true,
					loop: true,
					pauseOnHover: true,
					centerActive: true,
					stopAfterAction: true,
					hideNav: false,
					navOnHover: true,
					hidePager: false,
					pagerOnHover: true,
					swipable: true,
					expand: true,
					alwaysFill: true
				};
			}
		}
	});
	
	$scope.$watchCollection('wbModel.query', function(){
		loadAssets();
	});
	// Global functions
});