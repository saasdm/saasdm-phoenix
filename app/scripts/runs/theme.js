//'use strict';
//
//angular.module('saasdmPhoenixApp')//
//.run(function ($app, $rootScope, $mdTheming, themeProvider) {
//
//	function loadConfig(){
//		$app.config('themepalettes')//
//		.then(function(options){
//			var ctrl = options;
//			if(typeof ctrl === 'undefined'){
//				return;
//			}
//			setPrimaryPalette(ctrl.primary);
//			setAccentPalette(ctrl.accent);
//			setWarnPalette(ctrl.warn);
//			setBackgroundPalette(ctrl.background);
//			
//			//reload the theme
//			$mdTheming.generateTheme('default');
//
//			//optional - set the default to this new theme
//			themeProvider.setDefaultTheme('default');
//		});
//	}
//
//	function setPrimaryPalette(paletteStr){
//		if(paletteStr){			
//			var primaryPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customPrimary', primaryPalette);
//			themeProvider.theme('default').primaryPalette('customPrimary');
//		}
//	}
//
//	function setAccentPalette(paletteStr){
//		if(paletteStr){			
//			var accentPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customAccent', accentPalette);
//			themeProvider.theme('default').accentPalette('customAccent');
//		}
//	}
//
//	function setWarnPalette(paletteStr){		
//		if(paletteStr){			
//			var warnPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customWarn', warnPalette);
//			themeProvider.theme('default').warnPalette('customWarn');
//		}
//	}
//
//	function setBackgroundPalette(paletteStr){
//		if(paletteStr){			
//			var backgroundPalette = JSON.parse(paletteStr);
//			themeProvider.definePalette('customBackground', backgroundPalette);
//			themeProvider.theme('default').backgroundPalette('customBackground');
//		}
//	}
//
//	loadConfig();
//
//});