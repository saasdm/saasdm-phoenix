/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')
/*
 * ماشین حالت نرم افزار
 */
.config(function($routeProvider) {
	$routeProvider//
//	 .when('/', {
//		 templateUrl : 'views/home.html',
//		 controller : 'HomeCtrl',
//	 })//
	.when('/assets', {
		templateUrl : 'views/folder.html',
		controller : 'AssetsCtrl'
	})//
	.when('/assets/:query', {
		templateUrl : 'views/folder.html',
		controller : 'AssetsCtrl'
	})//
	.when('/folder/:folderId', {
		templateUrl : 'views/folder.html',
		controller : 'FolderCtrl'
	})//
	.when('/category/:categoryId', {
		templateUrl : 'views/category.html',
		controller : 'CategoryCtrl'
	})//
	.when('/asset/:assetId', {
		templateUrl : 'views/asset.html',
		controller : 'AssetCtrl'
	})//
	.when('/asset/:assetId/link/:secureId', {
		templateUrl : 'views/link.html',
		controller : 'LinkCtrl'
	})//
	.when('/users/signup', {
		templateUrl : 'views/customize/signup.html',
		controller : 'SignupCtrl'
	})//
	.when('/users/reset-password', {
		templateUrl : 'views/customize/forgot-password.html',
		controller : 'PasswordCtrl'
	})//
	.when('/users/profile', {
		templateUrl : 'views/customize/profile.html',
		controller : 'ProfileCtrl'
	})//
	.when('/users/user-area', {
		templateUrl : 'views/customize/user-area.html',
		controller : 'UserAreaCtrl'
	})//
	.when('/events/:name', {
		templateUrl : 'views/collection.html',
		controller : 'CollectionCtrl'
	})//
	/**
	 * SPA Settings
	 */
	.when('/settings/social-networks', {
		templateUrl : 'views/spa-settings/social-networks.html',
	})//
	.when('/settings/profile-options', {
		templateUrl : 'views/spa-settings/profile-options.html',
		controller: 'SpaSettingProfileOptionsCtrl'
	})//
	.when('/settings/theme', {
		templateUrl : 'views/spa-settings/theme-palettes.html',
		controller: 'SpaSettingThemeCtrl'
	})//
	.otherwise({
		templateUrl: 'views/page-not-found.html',
		controller: 'SdpPageNotFoundCtrl'
	});
});
