/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')
/*
 * تعیین تم
 */
.config(function ($mdThemingProvider/*, $provide*/) {

//	$mdThemingProvider.generateThemesOnDemand(true);
//	$provide.value('themeProvider', $mdThemingProvider);

	var gPalette = {
			'50': 'e2f0f4',
			'100': 'b7d8e3',
			'200': '88bfd0',
			'300': '58a5bd',
			'400': '3491af',
			'500': '107ea1',
			'600': '0e7699',
			'700': '0c6b8f',
			'800': '096185',
			'900': '054e74',
			'A100': 'a3dbff',
			'A200': '70c7ff',
			'A400': '3db3ff',
			'A700': '24a9ff',
			'contrastDefaultColor': 'light',
			'contrastDarkColors': [
				'50',
				'100',
				'200',
				'300',
				'A100',
				'A200',
				'A400',
				'A700'
				],
				'contrastLightColors': [
					'400',
					'500',
					'600',
					'700',
					'800',
					'900'
					]
	};

	var aPalette = {
			'50': 'fce5e0',
			'100': 'f8bfb3',
			'200': 'f49580',
			'300': 'ef6a4d',
			'400': 'eb4a26',
			'500': 'e82a00',
			'600': 'e52500',
			'700': 'e21f00',
			'800': 'de1900',
			'900': 'd80f00',
			'A100': 'ffffff',
			'A200': 'eb4a26',
			'A400': 'e52500',
			'A700': 'd80f00',
			'contrastDefaultColor': 'light',
			'contrastDarkColors': [
				'50',
				'100',
				'200',
				'300'
				],
				'contrastLightColors': [
					'400',
					'500',
					'600',
					'700',
					'800',
					'900',
					'A100',
					'A200',
					'A400',
					'A700'
					]
	};

	var bPalette = {
			'50': 'fdfeff',
			'100': 'fbfdfe',
			'200': 'f8fbfe',
			'300': 'f5f9fe',
			'400': 'f2f8fd',
			'500': 'f0f7fd',
			'600': 'd0e0e3',
			'700': 'a2c4c9',
			'800': '76a5af',
			'900': '45818e',
			'A100': 'ffffff',
			'A200': 'ffffff',
			'A400': 'ffffff',
			'A700': 'ffffff',
			'contrastDefaultColor': 'dark',
			'contrastDarkColors': [
				'50',
				'100',
				'200',
				'300',
				'400',
				'500',
				'600',
				'700',
				'A100',
				'A200',
				'A400',
				'A700'
				],
				'contrastLightColors': [
					'800',
					'900'
					]
	};

	$mdThemingProvider.definePalette('hitechPrimary', gPalette);
	$mdThemingProvider.definePalette('hitechAccent', aPalette);
	$mdThemingProvider.definePalette('hitechBackground', bPalette);

//	 $mdThemingProvider.theme('hitechTheme')//
//	    .primaryPalette('hitechPrimary')//
//	    .accentPalette('hitechAccent');
	
	$mdThemingProvider.theme('default').primaryPalette('hitechPrimary');
	$mdThemingProvider.theme('default').accentPalette('hitechAccent');
//	$mdThemingProvider.theme('default').backgroundPalette('hitechBackground');
	
//	$mdThemingProvider.setDefaultTheme('hitechTheme');

//	$mdThemingProvider.alwaysWatchTheme(true);
});

