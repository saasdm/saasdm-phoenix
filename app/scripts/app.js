'use strict';

/**
 * @ngdoc overview
 * @name saasdmPhoenixApp
 * @description # saasdmPhoenixApp
 * 
 * Main module of the application.
 */
angular.module('saasdmPhoenixApp', [
    'pluf.dm',//
    'ngMaterialHome',//
    'ngMaterialHomeUser',//
    'ngMaterialHomeBank',//
    'ngMaterialWeburgerCommon',//
    'ngMaterialWeburgerMailchimp',//
    'ngMaterialWeburgerSeenCollection',//
    'amWbCarousel',//
    'angular-kaarousel'
]);