/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc directive
 * @name saasdmPhoenixApp.directive:sdpCategoryDropdown
 * @description # sdpCategoryDropdown
 */
.directive('sdpCategoryDropdown', function() {
    return {
	restrict : 'E',
	replace : 'true',
	templateUrl : 'views/directives/categorydropdown.html',
	require : '^ngModel',
	scope : {
	    ngModel : '=?'
	},
	controller : function($scope, $sdp, PaginatorParameter) {
	    $scope.colorCodes = [ 
		'#7f78b7', '#e41278', '#00a2e1', '#71c269',
		'#4b848b', '#f9a450', '#c94f45', '#552060',
		'#34495e', '#fbbf48', '#494949', '#1f437d',
		'#d3844b'
	    ];
	    
	    $scope.childs = [];
	    var rootCategory = $scope.ngModel;
	    var pp = new PaginatorParameter();
	    pp.setFilter('parent', rootCategory.id);
	    pp.setSize(50);
	    pp.setOrder('id', 'a');
	    // load sub categories
	    $sdp.categories(pp).//
	    // rootCategory.categories(pp).//
	    then(function(pag){
		$scope.childs = pag.items;
	    });
	}
    };
});
