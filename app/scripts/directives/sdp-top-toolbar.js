/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc directive
 * @name saasdmPhoenixApp.directive:sdpAssetDropdown
 * @description # sdpAssetDropdown
 */
.directive('sdpTopToolbar', function() {

	function postLink(scope) {

		/*
		 * مرتب سازی مجدد داده‌ها بر اساس حالت فعلی 
		 */
		function search(searchText){
			if(!angular.isFunction(scope.sdpSearch)){
				return;
			}
			scope.sdpSearch(searchText);
		}

		function init(){
			if(angular.isFunction(scope.sdpSearch)){
				scope.search = search;
				scope.sdpEnableSearch = true;
			}
		}

		init();

	}

	return {
		restrict : 'E',
		replace : 'true',
		templateUrl : 'views/directives/top-toolbar.html',
		scope : {
			/*
			 * تابعی را تعیین می‌کند که باید برای جستجو فراخوانی شود.
			 */
			sdpSearch : '=',
			/*
			 * فهرستی از آیتم‌هایی که می‌خواهیم به این نوار ابزار اضافه کنیم
			 */
			sdpItems: '=',
		},
		link : postLink
	};
});
