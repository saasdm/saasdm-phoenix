/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc directive
 * @name saasdmPhoenixApp.directive:sdpAssetFilter
 * @description # sdpAssetFilter
 */
.directive('sdpAssetFilter', function() {
	return {
		restrict : 'E',
		templateUrl : 'views/directives/assetfilter.html',
		require : '^ngModel',
		scope : {
			ngModel : '=?',
			action : '=?',
			direction : '=?'
		},
		controller : function() {

		}
	};
});
