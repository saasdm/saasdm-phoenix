/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc directive
 * @name saasdmPhoenixApp.directive:sdpAssetSlider
 * @description # sdpAssetSlider
 */
.directive('sdpAssetSlider', function() {
    return {
	restrict : 'E',
	templateUrl : 'views/directives/assetslider.html',
	require : '^ngModel',
	scope : {
	    ngModel : '=?',
	    autoplay: '=?',
	    direction: '=?',
	    pauseOnHover: '=?',
	    centerActive: '=?',
	    timeInterval: '=?',
	    stopAfterAction: '=?',
	    hideNav: '=?',
	    hidePager: '=?',
	    navOnHover: '=?',
	    pagerOnHover: '=?',
	    swipable: '=?',
	    sync: '=?',
	    loop: '=?',
	    minWidth: '=?',
	    expand: '=?',
	    alwaysFill: '=?',
	    transitionDuration: '=?',
	    animation: '=?',
	    
	    displayed : '=?',
	    displayedXs : '=?',
	    displayedGtXs : '=?',
	    displayedSm : '=?',
	    displayedGtSm : '=?',
	    displayedMd : '=?',
	    displayedGtMd : '=?',
	    displayedLg : '=?',
	    displayedGtLg : '=?',
	    displayedXl : '=?',
	    perSlide : '=?',
	    perSlideXs : '=?',
	    perSlideGtXs : '=?',
	    perSlideSm : '=?',
	    perSlideGtSm : '=?',
	    perSlideMd : '=?',
	    perSlideGtMd : '=?',
	    perSlideLg : '=?',
	    perSlideGtLg : '=?',
	    perSlideXl : '=?',
	},
	controller : function($scope, $mdMedia) {
	    
	    $scope.$watch(function() {
		return $mdMedia('xs');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedXs ? $scope.displayedXs : $scope.displayed,
			perSlide : $scope.perSlideXs ? $scope.perSlideXs : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('gt-xs');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedGtXs ? $scope.displayedGtXs : $scope.displayed,
			perSlide : $scope.perSlideGtXs ? $scope.perSlideGtXs : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('sm');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedSm ? $scope.displayedSm : $scope.displayed,
			perSlide : $scope.perSlideSm ? $scope.perSlideSm : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('gt-sm');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedGtSm ? $scope.displayedGtSm : $scope.displayed,
			perSlide : $scope.perSlideGtSm ? $scope.perSlideGtSm : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('md');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedMd ? $scope.displayedMd : $scope.displayed,
			perSlide : $scope.perSlideMd ? $scope.perSlideMd : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('gt-md');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedGtMd ? $scope.displayedGtMd : $scope.displayed,
			perSlide : $scope.perSlideGtMd ? $scope.perSlideGtMd : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('lg');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedLg ? $scope.displayedLg : $scope.displayed,
			perSlide : $scope.perSlideLg ? $scope.perSlideLg : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('gt-lg');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedGtLg ? $scope.displayedGtLg : $scope.displayed,
			perSlide : $scope.perSlideGtLg ? $scope.perSlideGtLg : $scope.perSlide
		};
	    });
	    $scope.$watch(function() {
		return $mdMedia('xl');
	    }, function(val) {
		if (!val){
		    return;
		}
		$scope.conf = {
			displayed : $scope.displayedXl ? $scope.displayedXl : $scope.displayed,
			perSlide : $scope.perSlideXl ? $scope.perSlideXl : $scope.perSlide
		};
	    });
	}
    };
});
