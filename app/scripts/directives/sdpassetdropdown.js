/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('saasdmPhoenixApp')

/**
 * @ngdoc directive
 * @name saasdmPhoenixApp.directive:sdpAssetDropdown
 * @description # sdpAssetDropdown
 */
.directive('sdpAssetDropdown', function() {
    return {
	restrict : 'E',
	replace : 'true',
	templateUrl : 'views/directives/assetdropdown.html',
	require : '^ngModel',
	scope : {
	    ngModel : '=?'
	},
	controller : function($scope, $sdp, PaginatorParameter) {
	    
//	    var colors = [
//		{'r':0x7f, 'g':0x78, 'b':0xb7}, // #7f78b7
//		{'r':0xe4, 'g':0x12, 'b':0x78}, // #e41278
//		{'r':0x00, 'g':0xa2, 'b':0xe1}, // #00a2e1
//		{'r':0x71, 'g':0xc2, 'b':0x69}, // #71c269
//		{'r':0x4b, 'g':0x84, 'b':0x8b}, // #4b848b
//		{'r':0xf9, 'g':0xa4, 'b':0x50}, // #f9a450
//		{'r':0xc9, 'g':0x4f, 'b':0x45}, // #c94f45
//		{'r':0x55, 'g':0x20, 'b':0x60}, // #552060
//		{'r':0x34, 'g':0x49, 'b':0x5e}, // #34495e
//		{'r':0xfb, 'g':0xbf, 'b':0x48}, // #fbbf48
//		{'r':0x49, 'g':0x49, 'b':0x49}, // #494949
//		{'r':0x1f, 'g':0x43, 'b':0x7d}, // #1f437d
//		{'r':0xd3, 'g':0x84, 'b':0x4b}  // #d3844b
//	    ];
	    $scope.colorCodes = [ 
		'#7f78b7', '#e41278', '#00a2e1', '#71c269',
		'#4b848b', '#f9a450', '#c94f45', '#552060',
		'#34495e', '#fbbf48', '#494949', '#1f437d',
		'#d3844b'
	    ];
	    
	    $scope.childs = [];
	    var rootAsset = $scope.ngModel;
	    if(rootAsset.type === 'folder'){
		var pp = new PaginatorParameter();
		pp.setFilter('parent', rootAsset.id);
		pp.setSize(50);
		pp.setOrder('id', 'a');
		// load assets
		$sdp.assets(pp).//
		then(function(pag){
		    $scope.childs = pag.items;
		});
	    }
	    
//	    $scope.mouseEntered = function(event, asset){
//		var element = event.target;
//		var backgroundImage = 
//		    asset.thumbnail ? 'url(/api/cms/'+asset.thumbnail+'/download)' : 'none';
//		var index = asset.id%13; // Math.floor((Math.random() * 13) + 1);
//		var rgba = 'rgba('+colors[index].r+', '+colors[index].g+', '+colors[index].b+', 0.45)';
//		if(asset.thumbnail){		    
//		    element.style.background = 'linear-gradient('+rgba+', '+rgba+'),'+ backgroundImage;
//		}else{		    
//		    element.style.background = 'linear-gradient('+rgba+', '+rgba+')';
//		}
//	    };
//	    
//	    $scope.mouseExited = function(event, asset){
//		var element = event.target;
//		element.style.backgroundImage = 'none';
////		var index = asset.id%13; // Math.floor((Math.random() * 13) + 1);
////		var rgb = 'rgba('+colors[index].r+', '+colors[index].g+', '+colors[index].b+', 1.0)';
//		element.style.background = $scope.colorCodes[asset.id%13];
//	    };
	    
	}
    };
});
