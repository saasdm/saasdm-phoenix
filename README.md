# saasdm-phoenix

این پروژه یک SPA برای فراهم کردن امکانات زیر است:

- مشاهده فهرست asset ها
- مشاهده جزییات هر asset
- جستجو در asset ها
- دانلود asset مورد نظر

This project is generated with yo angular generator

## ساخت و توسعه

### ساختار

این پروژه با استفاده از yo angular generator ساختاردهی شده است.

### ساخت و پیش‌نمایش

برای ایجاد محصول نهایی پروژه دستور `grunt` را اجرا کنید.

برای پیش‌نمایش پروژه دستور `grunt serve` اجرا کنید.

## تست

دستور `grunt test` unit test ها را با karma اجرا می‌کند.
